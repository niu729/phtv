import '@umijs/max/typings';

declare global {
  interface IResponse {
    code: string;
    data: any;
    success: boolean;
    message: string;
  }

  interface castINF {
    // 真实信息
    person?: {
      id: number; // id编码
      name: string; // 名字
      image: {
        medium: string; // 图片
      };
    };
    // 角色信息
    character: {
      id: number;
      name: string;
      image: {
        medium: string;
      };
    };
  }

  interface TVColumnItem extends Record<string, any> {
    id?: number; // 番剧id
    name?: string; // 番剧名称
    type?: string; // 番剧类型
    rating?: {
      average: number | null; // 评分
    };
    image?: {
      medium: string; // 番剧图片
      original: string;
    };
    language?: string; // 语言
    premiered?: string; // 首次上映时间
    officialSite?: string; // 官方网站
    summary?: string; // 番剧简介
    runtime?: number; // 播放时长:分
    network?: {
      country: {
        name: string; // 国家
      };
    };
    _embedded?: {
      cast: castINF[];
    };
  }
}
