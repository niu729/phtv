# README

`@umijs/max` 模板项目，更多功能参考 [Umi Max 简介](https://umijs.org/docs/max/introduce)

### 启动前置

##### 建议使用 pnpm, node 版本 16+

##### 安装依赖

`pnpm install`

##### 启动项目

`pnpm start`

##### 打包项目,相关配置文件在 config 文件夹中

`测试环境: pnpm run build:test; 生产环境: pnpm run build`

##### docker 需要配置相关内容

`.sh 以及.sp1 文件中 中文部分需要进行相关配置才能使用脚本自动打包上传`
