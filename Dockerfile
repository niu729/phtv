FROM nginx:stable-alpine
# 设置时区为东八区
RUN echo -e http://mirrors.ustc.edu.cn/alpine/v3.15/main/ > /etc/apk/repositories
RUN apk add --no-cache tzdata && \
    # 设置时区
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" > /etc/timezone

# 安装 curl（如果基础镜像不包含 curl）
RUN apk add --no-cache curl

RUN rm /etc/nginx/conf.d/default.conf && mkdir -p /usr/local/nginx/html/phtv/dist
WORKDIR /usr/local/nginx/html/
ADD dist /usr/local/nginx/html/phtv/dist
COPY static.conf /etc/nginx/conf.d
ARG APP_VERSION_DATA
ENV NOTICE_SEND=$APP_VERSION_DATA
ENV NG_RUN="nginx -g \"daemon off;\""
RUN echo " $NOTICE_SEND && $NG_RUN "  > notic_send.sh
RUN chmod 777 notic_send.sh

CMD ["/bin/sh","-c","./notic_send.sh"]
EXPOSE 8080
