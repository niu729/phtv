const baseUrl = process.env.API_BASE || '';
// 列表接口
export const showsApi = `${baseUrl}/shows?page=`;
// 搜索接口
export const searchShowsApi = `${baseUrl}/search/shows?q=`;
// 查询详情接口
export const showsDetailApi = `${baseUrl}/shows/`;
