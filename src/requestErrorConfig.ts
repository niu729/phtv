﻿import type { RequestOptions } from '@@/plugin-request/request';
import type { RequestConfig } from '@umijs/max';
import { message as Message, notification } from 'antd';
import packageJson from '../package.json';
const version = packageJson.version;

notification.config({
  top: 100,
  duration: 0,
  closeIcon: null,
});

// // 与后端约定的响应数据格式
// interface ResponseStructure {
//   code: string;
//   data: any;
//   success?: boolean;
//   message: string;
//   requestId?: string;
// }

// 将 Node.js 流转换为字符串的帮助函数
// function streamToString(stream: any) {
//   console.log('stream => ', stream);
//   return new Promise((resolve, reject) => {
//     const chunks: any[] = [];
//     stream
//       .on('data', (chunk: any) => chunks.push(chunk))
//       .on('error', reject)
//       .on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
//   });
// }

/**
 * @name 错误处理
 * pro 自带的错误处理， 可以在这里做自己的改动
 * @doc https://umijs.org/docs/max/request#配置
 */
export const errorConfig: RequestConfig = {
  // 错误处理： umi@3 的错误处理方案。
  errorConfig: {
    // 错误接收及处理
    errorHandler: (error: any, opts: any) => {
      console.log(opts, error);
      const errorStatus = error?.response?.status;
      let message;
      switch (errorStatus) {
        case 302:
          message = '接口重定向了';
          break;
        case 400:
          message = '参数不正确';
          break;
        case 401: {
          // ! 处理登录
          break;
        }
        case 403:
          message = '您没有权限操作';
          break;
        case 404:
          message = `请求地址出错: ${error.response.config.url}`;
          break;
        case 408:
          message = '请求超时';
          break;
        case 409:
          message = '系统已存在相同数据';
          break;
        case 500:
          message = '服务器内部错误';
          break;
        case 501:
          message = '服务未实现';
          break;
        case 502:
          message = '网关错误';
          break;
        case 503:
          message = '服务不可用';
          break;
        case 504:
          message = '服务暂时无法访问，请稍后再试';
          break;
        case 505:
          message = 'HTTP版本不受支持';
          break;
        default:
          message = '异常问题，请联系管理员';
          break;
      }
      Message.error(message);
    },
  },

  // 请求拦截器
  requestInterceptors: [
    (config: RequestOptions) => {
      // 拦截请求配置，进行个性化处理。
      const info = window.localStorage.getItem('PHTV_USER_INFO') || '{}';
      const userInfo = JSON.parse(info) || {};

      (config as any).headers['token'] = userInfo?.token || '';
      (config as any).headers['web-version'] = version;
      return config;
    },
  ],

  // 响应拦截器
  responseInterceptors: [
    async (response: any) => {
      console.log('response => ', response);

      // // 检查是否是 gzip 压缩的数据
      // if (response.headers['content-encoding'] === 'gzip') {
      //   // 解压缩数据
      //   const unzipped = await streamToString(response.data);
      //   // 更新响应数据
      //   response.data = unzipped;
      //   // 删除 content-encoding 头部，避免后续处理程序误认为仍然是压缩数据
      //   delete response.headers['content-encoding'];
      // }
      return response;
    },
  ],
};
