import { kCollectionKey } from '@/constants';
import { useModel } from '@umijs/max';
import { Button, Image, Toast } from 'antd-mobile';
import { HeartFill, StarFill } from 'antd-mobile-icons';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import styles from './index.less';

interface IProps {
  id: number | null;
  name: string;
  type: string;
  image: string;
  country: string;
  runtime: number;
  premiered: string;
  rate: number;
  language: string;
  officialSite: string;
}

// PC组件
export const TVInfoPC = (
  props: IProps & {
    collected: boolean;
    handleCollect: () => void;
    handleRate: () => void;
  },
) => {
  return (
    <div className={styles.tvInfo}>
      <div className="desktop">
        <div className={classNames(styles.wrapper, 'clearfix')}>
          <div className={styles.infoLeft}>
            <div className={styles.avatarShadow}>
              <Image
                lazy
                className={styles.avatar}
                src={props.image}
                alt={props.name}
              />
            </div>
          </div>

          <div className={classNames(styles.infoRight, 'clearfix')}>
            <div className={styles.movieInfo}>
              <h1 className="name">{props.name}</h1>
              <div className={styles.infoBox}>
                {[
                  // type
                  props.type && (
                    <div className={styles.infoBoxItem} key="type">
                      type: {props.type}
                    </div>
                  ),
                  // language
                  props.language && (
                    <div className={styles.infoBoxItem} key="language">
                      language: {props.language}
                    </div>
                  ),
                  // country
                  <div className={styles.infoBoxItem} key="country">
                    {`${props.country ? `${props.country} / ` : ''}${
                      props.runtime ? `${props.runtime} min` : ''
                    } `}
                  </div>,
                  // premiered
                  props.premiered && (
                    <div className={styles.infoBoxItem} key="premiered">
                      premiered: {props.premiered}
                    </div>
                  ),
                  // officialSite
                  props.officialSite && (
                    <div className={styles.infoBoxItem} key="officialSite">
                      officialSite:{' '}
                      <a
                        href={props.officialSite}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {props.officialSite}
                      </a>
                    </div>
                  ),
                ]}
              </div>
            </div>
            <div className={styles.btns}>
              {[
                // 想看
                !props.collected && (
                  <Button
                    key="noCollected"
                    onClick={props.handleCollect}
                    className={styles.btn}
                  >
                    <HeartFill color="#fff" /> 想看
                  </Button>
                ),
                // 已想看
                props.collected && (
                  <Button
                    key="collected"
                    onClick={props.handleCollect}
                    className={styles.btn}
                  >
                    <HeartFill color="red" /> 已想看
                  </Button>
                ),
                // 评分
                <Button
                  key="rate"
                  className={styles.btn}
                  onClick={props.handleRate}
                >
                  <StarFill color="#fff" /> {props.rate || '暂无评分'}
                </Button>,
              ]}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

// Mobile组件
export const TVInfoMobile = (
  props: IProps & {
    collected: boolean;
    handleCollect: () => void;
    handleRate: () => void;
  },
) => {
  return (
    <div className={styles.tvInfoMobile}>
      <div className={styles.leftBox}>
        <Image
          lazy
          className={styles.medium}
          src={props.image}
          alt={props.name}
        />
      </div>
      <div className={styles.rightBox}>
        <h1 className={styles.name}>{props.name}</h1>
        <div className={styles.infoBox}>
          {[
            // type
            props.type && (
              <div className={styles.infoBoxItem} key="type">
                type: {props.type}
              </div>
            ),
            // language
            props.language && (
              <div className={styles.infoBoxItem} key="language">
                language: {props.language}
              </div>
            ),
            // country
            <div className={styles.infoBoxItem} key="country">
              {`${props.country ? `${props.country} / ` : ''}${
                props.runtime ? `${props.runtime} min` : ''
              } `}
            </div>,
            // premiered
            props.premiered && (
              <div className={styles.infoBoxItem} key="premiered">
                premiered: {props.premiered}
              </div>
            ),
            // officialSite
            props.officialSite && (
              <div className={styles.infoBoxItem} key="officialSite">
                officialSite:{' '}
                <a href={props.officialSite} target="_blank" rel="noreferrer">
                  {props.officialSite}
                </a>
              </div>
            ),
          ]}

          <div className={styles.btns}>
            {[
              // 想看
              !props.collected && (
                <Button
                  key="noCollected"
                  onClick={props.handleCollect}
                  className={styles.btn}
                >
                  <HeartFill color="#fff" /> 想看
                </Button>
              ),
              // 已想看
              props.collected && (
                <Button
                  key="collected"
                  onClick={props.handleCollect}
                  className={styles.btn}
                >
                  <HeartFill color="red" /> 已想看
                </Button>
              ),
              // 评分
              <Button
                key="rate"
                className={styles.btn}
                onClick={props.handleRate}
              >
                <StarFill color="#fff" /> {props.rate || '暂无评分'}
              </Button>,
            ]}
          </div>
        </div>
      </div>
    </div>
  );
};

export default function TVInfo(props: IProps) {
  const { initialState } = useModel('@@initialState');
  // 是否已收藏
  const [collected, setCollected] = useState(false);
  //

  // 处理用户是否已经收藏
  useEffect(() => {
    const collectIds = localStorage.getItem(kCollectionKey) || '[]';
    const collectIdsArr: any[] = JSON.parse(collectIds);
    setCollected(collectIdsArr.includes(`${props.id}`));
  }, [props.id]);

  // 处理喜欢
  const handleCollect = () => {
    const collectIds = localStorage.getItem(kCollectionKey) || '[]';
    const collectIdsArr: any[] = JSON.parse(collectIds);
    if (collectIdsArr.includes(`${props.id}`)) {
      // 如果已经收藏,再次点击则取消
      const newArr = collectIdsArr.filter((id) => id !== `${props.id}`);
      localStorage.setItem(kCollectionKey, JSON.stringify(newArr));
      Toast.show('已取消喜欢');
      setCollected(false);
    } else {
      // 如果没有收藏,则添加
      collectIdsArr.push(`${props.id}`);
      localStorage.setItem(kCollectionKey, JSON.stringify(collectIdsArr));
      setCollected(true);
    }
  };

  const handleRate = () => {
    Toast.show('评分功能暂未开放');
  };

  return initialState?.isMobile ? (
    <TVInfoMobile
      {...props}
      collected={collected}
      handleRate={handleRate}
      handleCollect={handleCollect}
    />
  ) : (
    <TVInfoPC
      {...props}
      collected={collected}
      handleRate={handleRate}
      handleCollect={handleCollect}
    />
  );
}
