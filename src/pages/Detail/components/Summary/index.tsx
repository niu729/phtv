import { useModel } from '@umijs/max';
import classNames from 'classnames';
import styles from './index.less';

interface IProps {
  summary: string;
}
export default function Summary(props: IProps) {
  const { initialState } = useModel('@@initialState');

  return (
    <div
      className={classNames(
        styles.summary,
        initialState?.isMobile ? 'mobile' : 'desktop',
        initialState?.isMobile && styles.summaryMobile,
      )}
    >
      <div className="section-header-title">summary</div>
      <div
        className={styles.desc}
        dangerouslySetInnerHTML={{ __html: props?.summary ?? '' }}
      ></div>
    </div>
  );
}
