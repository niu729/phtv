import { useModel } from '@umijs/max';
import { Image } from 'antd-mobile';
import classNames from 'classnames';
import styles from './index.less';

interface IProps {
  cast: castINF[];
}

export default function Cast(props: IProps) {
  const { initialState } = useModel('@@initialState');

  return (
    <div
      className={classNames(
        styles.cast,
        initialState?.isMobile ? 'mobile' : 'desktop',
        initialState?.isMobile && styles.castMobile,
      )}
    >
      <div className="section-header-title marginTop30">Cast</div>
      <div className={styles.castList}>
        {props.cast.map((item: castINF) => (
          <div className={styles.castItem} key={item.person?.id}>
            <Image
              lazy
              className={styles.castAvatar}
              src={item.person?.image?.medium}
              alt={item.person?.name}
            />
            <div className={styles.castInfo}>
              <div className={classNames(styles.castName, 'ellipsis')}>
                {item?.person?.name}
              </div>
              <div className={classNames(styles.castCharacter, 'ellipsis')}>
                as {item?.character?.name}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
