import { queryTVDetail } from '@/services';
import { useParams } from '@umijs/max';
import { Skeleton } from 'antd-mobile';
import { useCallback, useEffect, useState } from 'react';
import Cast from './components/Cast';
import Summary from './components/Summary';
import TVInfo from './components/TVInfo';
import styles from './index.less';

export default function Detail() {
  const { id = '' } = useParams();
  const [loading, setLoading] = useState(false);
  const [detail, setDetail] = useState<TVColumnItem>({});

  const queryDetail = useCallback(async () => {
    if (!id) return;
    setLoading(true);
    const res: any = await queryTVDetail(id);
    setDetail(res);
    document.title = res?.name || '仿TV';
    setLoading(false);
  }, [id]);

  useEffect(() => {
    queryDetail();
  }, [queryDetail]);

  return (
    <div className={styles.container}>
      {loading && <Skeleton />}

      {!loading &&
        detail?.name && [
          // 一些基础信息
          <TVInfo
            key="info"
            id={detail?.id ?? null}
            name={detail.name ?? ''}
            type={detail.type ?? ''}
            rate={detail?.rating?.average ?? 0}
            country={detail?.network?.country?.name ?? ''}
            runtime={detail.runtime ?? 0}
            premiered={detail.premiered ?? ''}
            image={detail?.image?.medium ?? ''}
            language={detail?.language ?? ''}
            officialSite={detail?.officialSite ?? ''}
          />,
          // 剧情简介
          <Summary key="summary" summary={detail?.summary ?? ''} />,
          // 演职人员
          <Cast key="cast" cast={detail?._embedded?.cast ?? []} />,
        ]}
    </div>
  );
}
