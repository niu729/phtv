import VideoCard from '@/components/VideoCard';
import { queryTVColumnList, queryTVSearchResults } from '@/services';
import { history, useModel } from '@umijs/max';
import { InfiniteScroll } from 'antd-mobile';
import classNames from 'classnames';
import { useCallback, useEffect, useState } from 'react';
import styles from './index.less';

const HomePage: React.FC = () => {
  const [data, setData] = useState<TVColumnItem[]>([]);
  const [pageNum, setPageNum] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const { initialState } = useModel('@@initialState');

  // 加载更多详情数据
  async function loadMore() {
    const res: any = await queryTVColumnList(pageNum);
    if (Array.isArray(res)) {
      // 如果从0页开始加载,源数组需要清空
      if (pageNum === 0) setData([...res]);
      else setData([...data, ...res]);

      setHasMore(res.length > 0);
      setPageNum(pageNum + 1);
    }
  }

  // 点击事件
  const cardItemOnClick = (item: any) => {
    console.log(item, '>>> click');
    history.push(`/detail/${item.id}`);
  };
  // 处理搜索
  const handleSearch = useCallback(async () => {
    console.log(initialState?.searchValue, '>>> search');
    if (initialState?.searchValue) {
      const res: any = await queryTVSearchResults(initialState?.searchValue);
      const resData = (res || []).map((item: any) => {
        return item.show;
      });
      setData([...resData]);
      setHasMore(false);
      setPageNum(0);
    } else {
      // 如果搜索内容被清空,搜索时重置数据从0页开始
      setHasMore(true);
      setData([]);
      loadMore();
    }
  }, [initialState?.searchValue]);

  useEffect(() => {
    handleSearch();
  }, [handleSearch]);

  return (
    <div
      className={classNames(
        styles.container,
        initialState?.isMobile ? 'mobile' : 'desktop',
      )}
    >
      <div
        className={classNames(
          styles.list,
          initialState?.isMobile && styles.mobile,
        )}
      >
        {data.map((item) => {
          return (
            <VideoCard
              key={item.id}
              img={item?.image?.medium ?? ''}
              title={item?.name ?? ''}
              type={item?.type ?? ''}
              rate={item?.rating?.average ?? 0}
              className={styles.listItem}
              onClick={() => cardItemOnClick(item)}
            />
          );
        })}
      </div>

      <InfiniteScroll loadMore={loadMore} hasMore={hasMore} />
    </div>
  );
};

export default HomePage;
