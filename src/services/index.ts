import { searchShowsApi, showsApi, showsDetailApi } from '@/apis';
import request from '@/utils/httpRequest';

// 请求TV栏目列表
export const queryTVColumnList = async (page: number) =>
  request(showsApi + page);

// 请求TV搜索结果列表
export const queryTVSearchResults = async (q: string) =>
  request(searchShowsApi + q);

// 请求TV详情
export const queryTVDetail = async (id: string) =>
  request(showsDetailApi + id + '?embed=cast');
