// 运行时配置
import logo from '@/assets/images/logo.jpg';
import { RunTimeLayoutConfig, history } from '@umijs/max';
import { Button, SearchBar, SearchBarRef } from 'antd-mobile';
import classNames from 'classnames';
import { useRef } from 'react';
import { isMobile } from './utils';

// 全局初始化数据配置，用于 Layout 用户信息和权限初始化
// 更多信息见文档：https://umijs.org/docs/api/runtime-config#getinitialstate
export async function getInitialState(): Promise<{
  name?: string;
  isMobile: RegExpMatchArray | null;
  searchValue?: string;
}> {
  return {
    name: '仿TV',
    isMobile: isMobile(),
    searchValue: '',
  };
}

export const layout: RunTimeLayoutConfig = ({
  initialState,
  setInitialState,
}) => {
  const isMobile = initialState?.isMobile;
  const searchRef = useRef<SearchBarRef>(null);

  // ! 显示搜索按钮的路由: 目前只有首页
  const showSearchRoutes = ['/home'];
  // 搜索
  const onSearch = (val: string) => {
    setInitialState((s: any) => ({ ...s, searchValue: val ?? '' }));
  };
  // 判断是否显示search框
  const showSearch = showSearchRoutes.find(
    (item) => location.pathname.indexOf(item) >= 0,
  );

  return {
    layout: 'top',
    fixedHeader: true,
    style: { backgroundColor: '#f5f5f5' },
    contentStyle: { backgroundColor: 'transparent !important', padding: 0 },
    headerRender: () => (
      <div
        className={classNames(isMobile ? 'mobile' : 'desktop', 'flex-between')}
      >
        {/* logo点击跳转首页 */}
        <div className="logo" onClick={() => history.replace('/home')}>
          <img src={logo} alt="logo" />
        </div>

        {showSearch && (
          <div className="search-box">
            <SearchBar
              ref={searchRef}
              onSearch={onSearch}
              className="search-input"
              showCancelButton={false}
              placeholder="请输入关键字搜索"
            />
            <Button
              color="primary"
              className="search-btn"
              onClick={() => {
                const val = searchRef.current?.nativeElement?.value || '';
                onSearch(val);
              }}
            >
              搜索
            </Button>
          </div>
        )}
      </div>
    ),
    rightContentRender: () => <div />,
  };
};

/**
 * @name request 配置，可以配置错误处理
 * 它基于 axios 和 ahooks 的 useRequest 提供了一套统一的网络请求和错误处理方案。
 * @doc https://umijs.org/docs/max/request#配置
 */
// export const request = {
//   // baseURL: reqHostName(),
//   ...errorConfig,
// };
