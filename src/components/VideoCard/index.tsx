import img_placeholder from '@/assets/img_placeholder.png';
import { Image, Rate, Space } from 'antd-mobile';
import classnames from 'classnames';
import styles from './index.less';

interface IProps {
  img: string; // 图片
  alt?: string; // 图片描述
  title: string; // 标题
  rate: number | null; // 评分
  type: string; // 类型
  className?: string;
  onClick: () => void;
}

export default function VideoCard(props: IProps) {
  // 占位图+失败时候显示的图片
  const ImgPlaceholder = () => (
    <div className={styles.imgPlaceholder}>
      <img src={img_placeholder} />
    </div>
  );

  return (
    <div
      className={classnames(styles.videoCard, props.className || '')}
      onClick={props.onClick}
    >
      <div className={styles.videoCardImg}>
        <Image
          lazy
          alt=""
          src={props.img}
          fallback={<ImgPlaceholder />}
          placeholder={<ImgPlaceholder />}
        />
        {props.img && <div className={styles.videoCardType}>{props.type}</div>}
      </div>

      <div className={styles.videoCardInfo}>
        <div className={styles.videoCardTitle}>{props.title}</div>
        <div className={styles.videoCardRate}>
          <Space align="center">
            <Rate readOnly count={1} className={styles.rate} />
            <div>{props.rate || '暂无评分'}</div>
          </Space>
        </div>
      </div>
    </div>
  );
}
