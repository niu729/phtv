/** @name 域名处理 */
export const reqHostName = () => {
  const hostname = `${process.env.API_BASE || window.location.origin}${
    process.env.API_PREFIX
  }`;
  console.log('环境域名: ', process.env, hostname);
  return hostname;
};

// 判断是否为移动端
export const isMobile = () => {
  return window.navigator.userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i,
  );
};
