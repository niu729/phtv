import { Toast } from 'antd-mobile';

export default async function (url: string, init?: RequestInit | undefined) {
  try {
    const res = await fetch(url, init);
    if (!res?.ok) throw new Error('请求失败');
    else {
      const json = await res.json();
      return Promise.resolve(json);
    }
  } catch (error) {
    console.log('err => ', error);
    if (error) {
      // 处理请求失败
      Toast.show({
        icon: 'fail',
        content: '请求失败,请稍后再试!',
        duration: 2000,
      });
    }
    return Promise.reject('请求失败,请稍后再试!');
  }
}
