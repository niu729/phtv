import { defineConfig } from '@umijs/max';
import routes from './routes';

const { UMI_ENV = 'dev' } = process.env;

export default defineConfig({
  base: '/phtv/',
  publicPath: '/phtv/',
  title: '仿TV',
  antd: {},
  access: {},
  model: {},
  initialState: {},
  request: {},
  layout: {
    title: '仿TV',
  },
  routes,
  plugins: [],
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd-mobile',
        style: false,
        libraryDirectory: 'es/components',
      },
      'antd-mobile',
    ],
  ],
  npmClient: 'pnpm',
  /**
   * @name moment 的国际化配置
   * @description 如果对国际化没有要求，打开之后能减少js的包大小
   * @doc https://umijs.org/docs/api/config#ignoremomentlocale
   */
  ignoreMomentLocale: true,
  /**
   * @name 快速热更新配置
   * @description 一个不错的热更新组件，更新时可以保留 state
   */
  fastRefresh: true,
  mfsu: {
    strategy: 'normal',
  },
  esbuildMinifyIIFE: true,
  favicons: UMI_ENV !== 'dev' ? ['/phtv/favicon.ico'] : ['favicon.ico'],

  // less配置
  // lessLoader: {
  //   modifyVars: {
  //     // hack: `true; @import "~@/styles/antd-mobile.less";`,
  //   },
  // },

  // 代理
  // proxy: {
  //   '/api': {
  //     target: 'https://www.tvmaze.com',
  //     changeOrigin: true,
  //     pathRewrite: { '^/api': '' },
  //   },
  // },
});
