import { defineConfig } from '@umijs/max';

export default defineConfig({
  define: {
    'process.env': {
      API_BASE: 'https://api.tvmaze.com',
      API_PREFIX: '/api',
      NODE_ENV: 'dev',
    },
  },
});
