# 使用 git rev-parse 命令获取当前分支名称
$currentBranch = git rev-parse --abbrev-ref HEAD

git pull

# 输出当前分支名称
Write-Host ""
Write-Host "current branch name is: $currentBranch"
Write-Host ""



do {
    $Evn = Read-Host "please choose build env num: [0]txtest [1]txprod"
    if (-not ("0","1" -contains $Evn) ){
        Write-Host "you choose build env is error,please input retry "
    }
} until ("0","1" -contains $Evn)


if ($Evn.Equals("0"))
{
    $EvnString = "txtest"
}elseif ($Evn.Equals("1"))
{
    $EvnString = "txprod"
}



Write-Host ""
Write-Host "you choose build env is,[$Evn] $EvnString!"
Write-Host ""


$ProjectName = "phtv"

# Get the current timestamp in yyyyMMddHHmmss format
$Timestamp = Get-Date -Format "yyyyMMddHHmmss"
$TagName = -Join($ProjectName,"-",$EvnString,":","$currentBranch-$Timestamp")
$ImageName = $TagName.Replace(":", "-") # Docker image tag cannot contain colons
$DockerfilePath = ".\Dockerfile"

$APP_VERSION = "$currentBranch-$Timestamp"

$TempPath = "./dist"

if (Test-Path -Path $TempPath -PathType Container) {
  Remove-Item -Path $TempPath -Recurse -Force
}
New-Item -ItemType Directory -Path $TempPath -Force

# build
Write-Host "Building  $ProjectName  ..."
cnpm install
if($Evn.Equals("0")){
  cnpm run build:test
}else{
  cnpm run build
}

if ($LASTEXITCODE -ne 0) {
    Write-Error "npm build failed with exit code $LASTEXITCODE."
    exit $LASTEXITCODE
}

$APP_VERSION_DATA = "生产环境 前端系统 启动成功，当前版本号:$APP_VERSION"
# 环境变量
$prefix="环境变量"
#测试
if($Evn.Equals("0")){
    $prefix="环境变量-test"
    $APP_VERSION_DATA = "测试环境 前端系统 启动成功，当前版本号:$APP_VERSION"

}

$APP_JSON =@"
{"msgtype": "text","text": {"content": "$APP_VERSION_DATA"}}
"@


$APP_VERSION_NOTICE = "curl -X POST -H 'Content-Type:application/json' -d '${APP_JSON}' 要推送到的链接地址"
echo $APP_VERSION_NOTICE

# 镜像
$remote_repository_tag="$prefix/xxx:$ImageName"
# Build the Docker image
Write-Host "Building Docker image with tag $remote_repository_tag ..."
docker build -t $remote_repository_tag -f $DockerfilePath --build-arg APP_VERSION_DATA=$APP_VERSION_NOTICE .

# Check if the build was successful
if ($LASTEXITCODE -ne 0) {
    Write-Error "Failed to build the Docker image."
    exit 1
}


if (Test-Path -Path $TempPath -PathType Container) {
    Remove-Item -Path $TempPath -Recurse -Force
}

docker login --username 姓名 --password 密码 域名地址

# 推送镜像
echo "Pushing image to $remote_repository_tag..."
docker push "$remote_repository_tag"

docker logout

echo ""
echo "delete local image"
docker rmi "$remote_repository_tag"


$development_url = "测试环境部署地址"
if ($Evn.Equals("0"))
{
  echo "测试环境部署地址: $development_url"

}elseif ($Evn.Equals("1"))
{
  $development_url = "生产环境部署地址"
  echo "生产环境部署地址: $development_url"
}
Start-Process -FilePath "$development_url"

echo "Done."


