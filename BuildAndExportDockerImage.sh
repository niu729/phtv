#!/bin/bash

# 使用 git rev-parse 命令获取当前分支名称
currentBranch=$(git rev-parse --abbrev-ref HEAD)

git pull

# 输出当前分支名称
echo ""
echo "current branch name is: $currentBranch"
echo ""

while true; do
    read -p "please choose build env num: [0]txtest [1]txprod " Evn
    if [[ ! "0 1" =~ $Evn ]]; then
        echo "you choose build env is error, please input retry "
    else
        break
    fi
done

if [ "$Evn" -eq 0 ]; then
    EvnString="txtest"
else
    EvnString="txprod"
fi

echo ""
echo "you choose build env is, [$Evn] $EvnString!"
echo ""

ProjectName="phtv"
Timestamp=$(date +%Y%m%d%H%M%S)
TagName="${ProjectName}-${EvnString}:${currentBranch}-${Timestamp}"
ImageName="${TagName//:/-}"  # 替换冒号避免 Docker 标签非法字符

APP_VERSION="${currentBranch}-${Timestamp}"
APP_VERSION_DATA=$(cat << EOF
{
    "msgtype": "text",
    "text": {
        "content": "$APP_VERSION"
    }
}
EOF
)

TempPath="./dist"

if [ -d "$TempPath" ]; then
    rm -rf "$TempPath"
fi
mkdir -p "$TempPath"

# build
echo "Building $ProjectName ..."
cnpm install
if [ "$Evn" -eq 0 ]; then
    cnpm run build:test
else
    cnpm run build
fi

if [ $? -ne 0 ]; then
    echo "npm build failed with exit code $?"
    exit $?
fi

### 环境变量
prefix="环境变量"
if [ "$Evn" -eq 0 ]; then
    prefix="环境变量-test"
fi

### 镜像
remote_repository_tag="${prefix}/xxx:${ImageName}"

# Build the Docker image
echo "Building Docker image with tag $remote_repository_tag ..."
docker build -t "$remote_repository_tag" -f Dockerfile --build-arg APP_VERSION_DATA="$APP_VERSION" .

# Check if the build was successful
if [ $? -ne 0 ]; then
    echo "Failed to build the Docker image."
    exit 1
fi

if [ -d "$TempPath" ]; then
    rm -rf "$TempPath"
fi

docker login --username 姓名 --password 密码 域名地址

# 推送镜像
echo "Pushing image to $remote_repository_tag..."
docker push "$remote_repository_tag"

docker logout

echo "测试环境部署地址 https://xxx.com/xxxx"

echo "Done."
